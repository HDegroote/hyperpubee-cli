const util = require('node:util')
const exec = util.promisify(require('node:child_process').exec)
const { expect } = require('chai')
const temp = require('temp')
const fs = require('fs').promises
const path = require('node:path')

temp.track() // Remove temp files after use

describe('CLI tests', function () {
  let inputPath

  this.beforeEach(async function () {
    const dirPath = await temp.mkdir('cli_tests')
    inputPath = path.join(dirPath, 'input.tex')

    const poemTxt = '[poetry]\nMy poem\n\nThis is\nA poem\n'
    await fs.writeFile(inputPath, poemTxt)

    process.env.hyperpubee_dbConnectionStr = ':memory:'
    process.env.hyperpubee_corestoreLoc = path.join(dirPath, 'test-corestore')
  })

  this.afterEach(async function () {
    await temp.cleanup()
  })

  it('Can create a work', async function () {
    const command = `node ./index create ${inputPath} myPoem`
    const { stdout, stderr } = await exec(command)

    expect(stdout).to.include("Successfully created work with identifier 'myPoem' (")
    expect(stderr).to.equal('')
  })

  it('Can create a work with an author', async function () {
    const command = `node ./index create ${inputPath} myPoem --author FitzG`
    const { stdout, stderr } = await exec(command)

    expect(stdout).to.include("Successfully created work with identifier 'myPoem' by author 'FitzG' (")
    expect(stderr).to.equal('')
  })
})
