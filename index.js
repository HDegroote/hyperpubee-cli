const fsPromises = require('fs').promises

const { program } = require('commander')
const { loadConfig, DbApp } = require('hyperpubee-backend')

async function runCreateFileCli (app, fileLoc, uniqueIdentifier, { author }) {
  const text = await fsPromises.readFile(fileLoc, 'utf-8')

  try {
    const work = await app.createWork(text, uniqueIdentifier, author)

    const authorInfo = author === undefined ? '' : ` by author '${author}'`

    console.log(
      [
        `Successfully created work with identifier '${uniqueIdentifier}'`,
        authorInfo,
        ` (${work.key})`
      ].join('')
    )
  } catch (error) {
    console.error(error)
    console.log(
      [
        'Note that the core might have been created before the error',
        'was thrown and no clean-up is done upon failure'
      ].join(' ')
    )
  }
}

async function main () {
  const config = loadConfig()
  const app = await DbApp.initFromConfig(config)

  program.name('hyperpubee').description('CLI for hyperpubee')

  program
    .command('create')
    .argument('<string>', 'File location')
    .argument('<string>', 'Unique identifier')
    .option('-a --author <string>', 'The author of the work')
    .action((...args) => runCreateFileCli(app, ...args))

  await program.parseAsync(process.argv)

  await app.close()
}

main()
