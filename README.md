# HyperPubee CLI

Command-line interface for managing [hyperpubees](https://gitlab.com/HDegroote/hyperpubee)

## Install
`npm install hyperpubee-cli`

## Usage
`node index.js create myWork.txt workIdentifier`

Where myWork.txt is a txt file with a work in the expected structure, and workIdentifier is a unique identifier for the work.

Config variables or a configuration file define which database and corestore you are interacting with. By default both are created in or loaded from your current folder. See [hyperpubee-backend](https://gitlab.com/HDegroote/hyperpubee-backend).
